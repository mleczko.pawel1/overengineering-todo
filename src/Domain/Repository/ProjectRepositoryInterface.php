<?php

declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Model\Project;

interface ProjectRepositoryInterface
{
    public function save(Project $project): void;

    public function findById(int $id): ?Project;
}
