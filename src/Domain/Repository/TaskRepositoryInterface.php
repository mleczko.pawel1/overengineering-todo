<?php

declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Model\Task;

interface TaskRepositoryInterface
{
    public function save(Task $task): void;
}
