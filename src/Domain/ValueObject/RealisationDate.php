<?php

declare(strict_types=1);

namespace App\Domain\ValueObject;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
final readonly class RealisationDate
{
    private function __construct(
        #[ORM\Column(type: 'date', nullable: true)]
        private DateTimeInterface|null $realisationDate
    ) {
    }

    public static function createRealisationDate(?DateTimeInterface $realisationDate): self
    {
        return new self($realisationDate);
    }

    public function changeRealisationDate(?DateTimeInterface $newRealisationDate): self
    {
        return new self($newRealisationDate);
    }

    public function getRealisationDate(): ?DateTimeInterface
    {
        return $this->realisationDate;
    }
}
