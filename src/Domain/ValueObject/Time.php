<?php

declare(strict_types=1);

namespace App\Domain\ValueObject;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
final readonly class Time
{
    private function __construct(
        #[ORM\Column(nullable: true)]
        private int|null $time
    ) {
    }

    public static function createTime(?int $time): self
    {
        return new self($time);
    }

    public function addTime(?int $time): self
    {
        return new self($this->time + $time);
    }

    public function getTime(): ?int
    {
        return $this->time;
    }
}
