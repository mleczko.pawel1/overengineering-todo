<?php

declare(strict_types=1);

namespace App\Domain\Enum;

enum Status: string
{
    case TODO = 'To do';
    case DONE = 'Done';
}
