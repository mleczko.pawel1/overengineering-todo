<?php

declare(strict_types=1);

namespace App\Domain\Model;

use App\Domain\Enum\Status;
use App\Domain\ValueObject\Name;
use App\Domain\ValueObject\RealisationDate;
use App\Domain\ValueObject\Time;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
final class Task
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    public function __construct(
        #[ORM\Embedded(class: Name::class)]
        private readonly Name $name,
        #[ORM\ManyToOne(targetEntity: Project::class, inversedBy: 'tasks')]
        #[ORM\JoinColumn(name: 'project', referencedColumnName: 'id')]
        private readonly Project|null $project,
        #[ORM\Column(type: Status::class)]
        private readonly Status $status,
        #[ORM\Embedded(class: Time::class)]
        private readonly Time $time,
        #[ORM\Embedded(class: RealisationDate::class)]
        private readonly RealisationDate $realisationDate
    ) {
    }
}
