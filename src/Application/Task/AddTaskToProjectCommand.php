<?php

declare(strict_types=1);

namespace App\Application\Task;

use App\Application\Interfaces\CommandInterface;
use App\Domain\Model\Project;

final class AddTaskToProjectCommand implements CommandInterface
{
    public function __construct(
        public readonly int $projectId,
        public readonly string $name,
        public readonly string $status,
        public readonly int|null $time,
        public readonly string|null $realisationDate
    ) {
    }
}
