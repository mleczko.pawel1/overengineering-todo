<?php

declare(strict_types=1);

namespace App\Application\Task;

use App\Application\Interfaces\CommandHandlerInterface;
use App\Application\Interfaces\CommandInterface;
use App\Domain\Enum\Status;
use App\Domain\Model\Task;
use App\Domain\Repository\ProjectRepositoryInterface;
use App\Domain\Repository\TaskRepositoryInterface;
use App\Domain\ValueObject\Name;
use App\Domain\ValueObject\RealisationDate;
use App\Domain\ValueObject\Time;

class AddTaskToProjectHandler implements CommandHandlerInterface
{
    public function __construct(
        private readonly TaskRepositoryInterface $taskRepository,
        private readonly ProjectRepositoryInterface $projectRepository
    ) {
    }

    public function handle(CommandInterface $command): void
    {
        $project = $this->projectRepository->findById($command->projectId);

        $name = Name::fromString($command->name);
        $time = Time::createTime($command->time);
        $realisationDate = RealisationDate::createRealisationDate($command->realisationDate ? new \DateTimeImmutable($command->realisationDate) : null);

        $task = new Task(
            $name,
            $project,
            Status::from($command->status),
            $time,
            $realisationDate
        );

        $project->addTask($task);

        $this->taskRepository->save($task);
    }
}
