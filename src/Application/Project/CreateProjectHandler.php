<?php

declare(strict_types=1);

namespace App\Application\Project;

use App\Application\Interfaces\CommandHandlerInterface;
use App\Application\Interfaces\CommandInterface;
use App\Domain\Model\Project;
use App\Domain\Repository\ProjectRepositoryInterface;
use App\Domain\ValueObject\Name;

class CreateProjectHandler implements CommandHandlerInterface
{
    public function __construct(
        private readonly ProjectRepositoryInterface $projectRepository
    ) {
    }

    public function handle(CommandInterface $command): void
    {
        $name = Name::fromString($command->name);

        $project = new Project($name);

        $this->projectRepository->save($project);
    }
}
