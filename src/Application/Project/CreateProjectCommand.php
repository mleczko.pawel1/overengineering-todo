<?php

declare(strict_types=1);

namespace App\Application\Project;

use App\Application\Interfaces\CommandInterface;

final class CreateProjectCommand implements CommandInterface
{
    public function __construct(
        public string $name
    ) {
    }
}
