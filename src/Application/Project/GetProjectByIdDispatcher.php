<?php

declare(strict_types=1);

namespace App\Application\Project;

use App\Application\Exception\ObjectNotFoundException;
use App\Application\Interfaces\DTOInterface;
use App\Application\Interfaces\QueryDispatcherInterface;
use App\Application\Interfaces\QueryInterface;
use App\Domain\Repository\ProjectRepositoryInterface;

class GetProjectByIdDispatcher implements QueryDispatcherInterface
{
    public function __construct(
        private readonly ProjectRepositoryInterface $projectRepository
    ) {
    }

    /**
     * @throws ObjectNotFoundException
     */
    public function dispatch(QueryInterface $query): DTOInterface
    {
        // Idealnie by było, gdyby już z repozytorium wracało konkretne DTO, co jest do zrobienia, ale w tym przypadku
        // postawiłem na takie rozwiązanie, które można w każdym momencie zmienić
        $project = $this->projectRepository->findById($query->id);

        if (! $project) {
            throw new ObjectNotFoundException();
        }

        return new ProjectIdDTO(
            $project->getId()
        );
    }
}
