<?php

declare(strict_types=1);

namespace App\Application\Project;

use App\Application\Interfaces\QueryInterface;

final class GetProjectByIdQuery implements QueryInterface
{
    public function __construct(
        public readonly int $id
    ) {
    }
}
