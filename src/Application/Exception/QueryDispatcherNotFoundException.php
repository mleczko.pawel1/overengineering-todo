<?php

declare(strict_types=1);

namespace App\Application\Exception;

use Exception;

class QueryDispatcherNotFoundException extends Exception
{
}
