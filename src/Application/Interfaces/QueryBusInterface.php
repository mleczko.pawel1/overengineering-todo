<?php

declare(strict_types=1);

namespace App\Application\Interfaces;

use App\Application\Exception\QueryDispatcherNotFoundException;

interface QueryBusInterface
{
    /**
     * @param QueryInterface $query
     * @return DTOInterface
     * @throws QueryDispatcherNotFoundException
     */
    public function dispatch(QueryInterface $query): DTOInterface;
}
