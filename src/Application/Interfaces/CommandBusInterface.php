<?php

declare(strict_types=1);

namespace App\Application\Interfaces;

use App\Application\Exception\CommandHandlerNotFoundException;

interface CommandBusInterface
{
    /**
     * @param CommandInterface $command
     * @return void
     * @throws CommandHandlerNotFoundException
     */
    public function handle(CommandInterface $command): void;
}
