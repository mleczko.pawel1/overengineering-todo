<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Model\Project;
use App\Domain\Repository\ProjectRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class ProjectRepository extends ServiceEntityRepository implements ProjectRepositoryInterface
{

    public function save(Project $project): void
    {
        // TODO: Implement save() method.
    }

    public function findById(int $id): ?Project
    {
        // TODO: Implement findById() method.
    }
}
