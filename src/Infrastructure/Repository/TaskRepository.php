<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Model\Task;
use App\Domain\Repository\TaskRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class TaskRepository extends ServiceEntityRepository implements TaskRepositoryInterface
{

    public function save(Task $task): void
    {
        // TODO: Implement save() method.
    }
}
