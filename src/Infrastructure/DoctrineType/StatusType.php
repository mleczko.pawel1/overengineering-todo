<?php

declare(strict_types=1);

namespace App\Infrastructure\DoctrineType;

use App\Domain\Enum\Status;

class StatusType extends AbstractEnumType
{
    public const NAME = 'status';

    public static function getEnumsClass(): string
    {
        return Status::class;
    }

    public function getName(): string
    {
        return self::NAME;
    }
}
