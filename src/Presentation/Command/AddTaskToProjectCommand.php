<?php

declare(strict_types=1);

namespace App\Presentation\Command;

use App\Application\Exception\CommandHandlerNotFoundException;
use App\Application\Exception\ObjectNotFoundException;
use App\Application\Interfaces\CommandBusInterface;
use App\Application\Interfaces\QueryBusInterface;
use App\Application\Project\GetProjectByIdQuery;
use App\Application\Task\AddTaskToProjectCommand as AddTaskToProjectApplicationCommand;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:project:add-task')]
class AddTaskToProjectCommand extends Command
{
    public function __construct(
        private readonly QueryBusInterface $queryBus,
        private readonly CommandBusInterface $commandBus
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('project_id');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $projectId = $input->getArgument('project_id');
        $getProjectByIdQuery = new GetProjectByIdQuery($projectId);

        try {
            $projectDto = $this->queryBus->dispatch($getProjectByIdQuery);
        } catch (ObjectNotFoundException) {
            $output->writeln('Project not found!');
            return Command::FAILURE;
        }

        //Getting params from user input about task
        $name = 'name';
        $status = 'Done';
        $time = null;
        $realisationDate = null;

        $command = new AddTaskToProjectApplicationCommand(
            $projectDto->getId(),
            $name,
            $status,
            $time,
            $realisationDate
        );

        try {
            $this->commandBus->handle($command);
            $output->writeln('Task added');
        } catch (CommandHandlerNotFoundException) {
            $output->writeln('ERROR!');
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
