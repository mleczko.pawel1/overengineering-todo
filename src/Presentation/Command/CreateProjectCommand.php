<?php

declare(strict_types=1);

namespace App\Presentation\Command;

use App\Application\Exception\CommandHandlerNotFoundException;
use App\Application\Interfaces\CommandBusInterface;
use App\Application\Project\CreateProjectCommand as CreateProjectApplicationCommand;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:project:create')]
class CreateProjectCommand extends Command
{
    public function __construct(
        private readonly CommandBusInterface $commandBus
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('project_name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $projectName = $input->getArgument('project_name');

        $createProjectCommand = new CreateProjectApplicationCommand($projectName);
        try {
            $this->commandBus->handle($createProjectCommand);
            $output->writeln('Project created');
        } catch (CommandHandlerNotFoundException) {
            $output->writeln('ERROR!');
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
